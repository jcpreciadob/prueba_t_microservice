from flask import Flask, request, jsonify  # Importa la biblioteca Flask y los métodos request y jsonify

app = Flask(__name__)

@app.route('/DevOps', methods=['POST'])
def ping_pong():
    data = request.get_json()  # Obtiene los datos JSON de la solicitud
    message = data.get('message')  # Obtiene el valor del campo 'message'
    
    if message == 'pong':
        response = {'message': 'ping pong'}
    else:
        response = {'message': 'Invalid request'}
    
    return jsonify(response)  # Devuelve la respuesta en formato JSON

@app.route('/health')
def health_check():
    return "¡Estoy saludable!", 200  # Retorna la respuesta con código de estado 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
