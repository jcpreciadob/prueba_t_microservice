import jwt
from flask import Flask, request, jsonify

app = Flask(__name__)

jwt_secret = 'secreto'  # Reemplaza esto con tu propio secreto

@app.route('/DevOps', methods=['POST'])
def devops_endpoint():
    # Verificar la presencia de la APIKey en los encabezados
    if 'X-Parse-REST-API-Key' not in request.headers:
        return 'ERROR', 401
    api_key = request.headers['X-Parse-REST-API-Key']
    
    # Verificar la APIKey
    if api_key != '2f5ae96c-b558-4c7b-a590-a501ae1c3f6c':
        return 'ERROR', 401
    
    # Verificar el tipo de contenido
    if not request.is_json:
        return 'ERROR', 400
    
    # Obtener los datos del cuerpo de la solicitud
    data = request.get_json()
    
    # Verificar los campos requeridos en los datos
    if 'message' not in data or 'to' not in data or 'from' not in data or 'timeToLifeSec' not in data:
        return 'ERROR', 400
    
    message = data['message']
    to = data['to']
    
    # Verificar el JWT
    if 'X-JWT-KWY' not in request.headers:
        return 'ERROR', 401
    jwt_token = request.headers['X-JWT-KWY']
    
    try:
        decoded_payload = jwt.decode(jwt_token, jwt_secret, algorithms=['HS256'])
    except jwt.InvalidTokenError:
        return 'ERROR', 401
    
    # Verificar los datos del JWT
    if 'message' not in decoded_payload or 'to' not in decoded_payload:
        return 'ERROR', 400
    
    # Comparar los datos del JWT con los datos del cuerpo de la solicitud
    if message != decoded_payload['message'] or to != decoded_payload['to']:
        return 'ERROR', 400
    
    # Construir la respuesta
    response_data = {
        'message': f'Hello {to} your message will be sent'
    }
    
    return jsonify(response_data)

if __name__ == '__main__':
    app.run()
