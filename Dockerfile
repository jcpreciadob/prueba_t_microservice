# Usa la imagen base de Python
FROM python:3.9.17-alpine3.18

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el archivo de código fuente al contenedor
COPY ping.py /app
COPY requeriments.txt /app

# Instala las dependencias del proyecto
RUN pip install --no-cache-dir -r requeriments.txt

# Expone el puerto en el que se ejecuta el microservicio
EXPOSE 80

# Establece el comando por defecto cuando se inicia el contenedor
CMD ["python", "ping.py"]
