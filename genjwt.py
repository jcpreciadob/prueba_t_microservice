import jwt

payload = {
    'message': 'This is a test',
    'to': 'Juan Perez',
    'from': 'Rita Asturia',
    'timeToLifeSec': 45
}

jwt_secret = 'Viviana2020*'  # Reemplaza esto con tu propio secreto

jwt_token = jwt.encode(payload, jwt_secret, algorithm='HS256')

print(jwt_token)

